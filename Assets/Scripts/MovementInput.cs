﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementInput : MonoBehaviour {

    /** --------------Other classes-----------------*/
    private BigLugage bigLugageScript;
    private SmallLugage smallLugageScript;
    private istrinti istrintiScript;
    /** -------------Other classes end--------------*/

    /** ----------------Keys------------------------*/
    public float InputX;
    public float InputZ;
    public float run;
    public float jump;
    public float crouch;
    public float pull;
    public float pushGrab;
    public float mouse;
    /** ------------Keys end------------------------*/

    public Animator animator;
    public Camera camera;
    public CharacterController controller;

    /**-----------------Walk-------------------------*/
    public Vector3 moveDirection = Vector3.zero;
    public Vector3 moveDirection2 = Vector3.zero;
    public float rotationSpeed;
    private float speed = 0f;
    private float walkStartTime;
    private float touchWallInputMagnitude;
    private bool touchWall;
    /**-----------------Walk End---------------------*/

    /**-----------------Run--------------------------*/
    /**-----------------Run End----------------------*/

    /**-----------------Jump-------------------------*/
    public bool isGrounded;
    public float jumpHeight = 8f;
    private float jumpTime;
    private float jumpBeginTime;
    private bool isJump = false;
    private bool endJump = false;
     private bool jumpBegin = false;
    private float yPosition;
    protected float m_VerticalSpeed = 1f;   
    public float jumpSpeed = 15f; 
    public float gravity = 20f;
    const float k_StickingGravityProportion = 0.3f;
    /**-----------------Jump End---------------------*/

    /**-----------------Crouch-----------------------*/
    private bool isCrouching = false;
    /**-----------------Crouch End-------------------*/

    /**-----------------Pull-------------------------*/
    private float inverseWalkDirection = 1f;
    private bool isPull = false;
    /**-----------------Pull End---------------------*/

    /**-----------------Push-------------------------*/
    private bool isPush = false;
    /**-----------------Push End---------------------*/

    /**-----------------Grab-------------------------*/
    private bool isGrab = false;
    public float handPositionY = 0f;
    public Vector3 headPosition;
    private float speedThenGrab = 1f;
    /**-----------------Grab End---------------------*/

    /**-----------------Throw------------------------*/
    public bool toThrow = false;
    private bool isThrow = false;
    /**-----------------Throw End--------------------*/

    public bool testing;
    
    
    
    // Use this for initialization
    void Start() {
        animator = this.GetComponent<Animator>();
        camera = Camera.main;
        controller = GetComponent<CharacterController>();

        bigLugageScript = FindObjectOfType<BigLugage>();
        smallLugageScript = FindObjectOfType<SmallLugage>();
        istrintiScript = FindObjectOfType<istrinti>();

        rotationSpeed = .3f;
        testing = false;

        controller.height = 10.48f;
        controller.center = new Vector3(0, 5.17f, 0.15f);
    }

    // Update is called once per frame
    void Update() {
        InputX = Input.GetAxis("Horizontal");
        InputZ = Input.GetAxis("Vertical");
        run = Input.GetAxis("Fire3");
        jump = Input.GetAxis("Jump");
        crouch = Input.GetAxis("Fire1");
        pull = Input.GetAxis("Pull");
        pushGrab = Input.GetAxis("PushGrab");
        mouse = Input.GetAxis("Mouse0");
        isGrounded = controller.isGrounded;



        /** -----------Pull ---------------------------------------*/

        if (bigLugageScript.grabBig)
        {
            inverseWalkDirection = -1f;
            rotationSpeed = 0.15f;
        }
        if (!bigLugageScript.grabBig)
        {
            inverseWalkDirection = 1f;
            rotationSpeed = 0.3f;
        }

        if (bigLugageScript.grabBig && !isPull) {
            animator.SetTrigger("pull");
            isPull = true;
        }

        if (!bigLugageScript.grabBig && isPull) {
            animator.SetTrigger("pullEnd");
            isPull = false;
        }
        /** -----------Pull End---------------------------------------*/

        /** -----------Push------------------------------------------*/
        if (istrintiScript.stumti && !isPush)
        {
            animator.SetTrigger("push");
            isPush = true;
        }

        if (!istrintiScript.stumti && isPush)
        {
            animator.SetTrigger("pushEnd");
            isPush = false;
        }

        /** -----------Push End--------------------------------------*/

        /** -----------Grab and Throw---------------------------------------*/

        if (pushGrab > 0 && !isGrab && smallLugageScript.playerTrigger && isGrounded) {
            animator.SetTrigger("grab");
            isGrab = true;
            speedThenGrab = 2f;
        }

        if (isGrab) {
            isGrounded = true;
        }

        if (pushGrab > 0 && isGrab)
        {
            animator.SetFloat("grabWalk", Mathf.Clamp(new Vector2(InputX, InputZ).sqrMagnitude, 0f, 1f), 0f, Time.deltaTime);

            if (pushGrab + mouse > 1 && !isThrow && smallLugageScript.playerTrigger)
            {
                animator.SetTrigger("throw");
                isThrow = true;
            }
        }

        if (pushGrab + mouse < 1 && isThrow)
        {
            toThrow = true;
            animator.SetTrigger("throwEnd");
            isThrow = false;
            isGrab = false;
        }

        if (pushGrab == 0 && isGrab) {
            animator.SetTrigger("grabEnd");
            isGrab = false;
            speedThenGrab = 1f;
        } 

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Grab End")) {
            Debug.Log("ddd");
            isGrounded = true;
        }
        
        Transform temp = animator.GetBoneTransform (HumanBodyBones.RightHand);
        handPositionY = temp.position.y;

        Transform head = animator.GetBoneTransform(HumanBodyBones.Head);
        headPosition = head.position;
        /** -----------Grab and Throw End---------------------------------------*/

        /** -----------Run---------------------------------------*/

        if (run != 0)
        {
            speed = Mathf.Clamp(new Vector3(InputX, InputZ, run).sqrMagnitude / 2, 0f, 1f); // nuo 0 iki 1
        }
        else if (run == 0)
        {
            speed = Mathf.Clamp(new Vector2(InputX, InputZ).sqrMagnitude / 2, 0f, 0.5f); // nuo 0 iki 0.5
        }
        /** -----------Run End---------------------------------------*/

        /** -----------Jump---------------------------------------*/

        if (jump == 1 && isGrounded)
        {
            animator.SetTrigger("jump");
            jumpBegin = true;
        } else if (!isJump && !isGrounded) {
            isJump = true;
            animator.SetTrigger("notGrounded");
        }
       
        if (jumpBegin) {
            jumpBeginTime += Time.deltaTime;
            isGrounded = false;
        }

        if (jumpBeginTime > .5f && jumpBegin) {
            isJump = true;
            m_VerticalSpeed = jumpSpeed;
            jumpBegin = false;
        }

        if (isJump) {
            jumpTime += Time.deltaTime;
        }

        if (isJump && isGrounded) // paleisti sokimo pabaigimo animacija
        {
            m_VerticalSpeed = 0f;

            if (jumpTime > 2f) {
                animator.SetTrigger("jumpEndDead");
            } else {
                animator.SetTrigger("jumpEnd");
            }                  
            
            isJump = false;
            jumpBeginTime = 0f;
            jumpTime = 0f;
        } 
        
        
        /** -----------Jump End---------------------------------------*/

         /** -----------Walk ---------------------------------------*/
        
        if ((InputX != 0 || InputZ != 0)) 
        {
            walkStartTime += Time.deltaTime * 1.5f;

            animator.SetFloat("InputZ", InputZ, 0.0f, Time.deltaTime);
            animator.SetFloat("InputX", InputX, 0.0f, Time.deltaTime);
            animator.SetFloat("InputMagnitude", speed);

            var forward = camera.transform.forward;
            var right = camera.transform.right;

            forward.y = 0f;
            right.y = 0f;

            

            moveDirection2 = forward * InputZ + right * InputX;

            if (!isJump) 
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(inverseWalkDirection * moveDirection2), rotationSpeed);
            

        }
        else if (InputX == 0 || InputZ == 0)
        {
            walkStartTime = 0f;
            speed = 0f;
            animator.SetFloat("InputMagnitude", speed);
        }
        /** -----------Walk End---------------------------------------*/

        /** ------------Crouch--------------------------------------- */
        
        if (crouch > 0 && !isCrouching) {
            animator.SetTrigger("crouch");
            isCrouching = true;
            controller.height = 7.32f;
            controller.center = new Vector3(0, 3.62f, 0.15f);
        }

        if (crouch == 0 && isCrouching) {
            animator.SetTrigger("crouchEnd");
            isCrouching = false;
            controller.height = 10.48f;
            controller.center = new Vector3(0, 5.17f, 0.15f);
        } 
        /** ------------Crouch end----------------------------------- */

        

        //moveDirection.y = moveDirection.y - (gravity * Time.deltaTime); // nuleisti zemyn

        // Add to the movement with the calculated vertical speed.
        
       
        moveDirection = inverseWalkDirection * transform.forward * speed * Time.deltaTime * 25f * speedThenGrab;
        if (!isJump) {
            moveDirection.y = moveDirection.y - (20f * Time.deltaTime); // nuleisti zemyn
        } else {
            moveDirection += m_VerticalSpeed * Vector3.up * Time.deltaTime;
        }

        controller.Move(moveDirection); // keisti pozicija kontrolerio

        /** -----------------When touch wall----------------------------*/
        float currentSpeed = controller.velocity.x + controller.velocity.z;

        if (currentSpeed != 0)
        {
            touchWallInputMagnitude = 0;
            touchWall = false;
        }
        if (currentSpeed == 0 && !touchWall)
        {
            touchWallInputMagnitude = speed;
            touchWall = true;
        }
        if (currentSpeed == 0 && touchWall && animator.GetFloat("InputMagnitude") > 0)
        {
            touchWallInputMagnitude = touchWallInputMagnitude - 0.03f;
            animator.SetFloat("InputMagnitude", touchWallInputMagnitude, 0f, Time.deltaTime);
        }
        /** -----------------When touch wall end----------------------------*/


        /*  RaycastHit hit;     // aukstis virs ground

        if (Physics.Raycast(transform.localPosition, transform.TransformDirection(Vector3.down), out hit))
        {
            heightAboveGround = hit.distance;
            
        }
        Debug.Log(heightAboveGround); */
    }
    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //Rigidbody body = hit.collider.attachedRigidbody;

        //// no rigidbody
        //if (body == null || body.isKinematic)
        //    return;

        //// We dont want to push objects below us
        //if (hit.moveDirection.y < -0.3f)
        //    return;

        //Vector3 forward = transform.TransformDirection(Vector3.forward);
        //Vector3 toOther = body.position - transform.position;
        ////Debug.Log(Vector3.Dot(forward.normalized, toOther.normalized));
        //if (Vector3.Dot(forward.normalized, toOther.normalized) >= 0.5f && Vector3.Dot(forward.normalized, toOther.normalized) <= 1)
        //{

        //    // Calculate push direction from move direction,
        //    // we only push objects to the sides never up and down
        //    Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);

        //    // If you know how fast your character is trying to move,
        //    // then you can also multiply the push velocity by that.

        //    // Apply the push
        //    body.velocity = pushDir * 2f;


        //}


    }
}
