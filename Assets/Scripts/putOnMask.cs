﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class putOnMask : MonoBehaviour {

    private bool playerTrigger;
    private ConfigurableJoint joint;
    private MeshCollider col;
    public float putOnMaskInput;
    public bool withMask = false;
    List<GameObject> theaterObjectsList;

    private MovementInput movementInputScript;

    // Use this for initialization
    void Start () {
        joint = gameObject.GetComponent<ConfigurableJoint>();
        col = gameObject.GetComponent<MeshCollider>();

        theaterObjectsList = new List<GameObject>();
        theaterObjectsList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Theater"));

        movementInputScript = FindObjectOfType<MovementInput>();
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButtonDown("PutOnMask") && withMask)
        {
            joint.xMotion = ConfigurableJointMotion.Free;
            joint.yMotion = ConfigurableJointMotion.Free;
            joint.zMotion = ConfigurableJointMotion.Free;
            joint.angularXMotion = ConfigurableJointMotion.Free;
            joint.angularYMotion = ConfigurableJointMotion.Free;
            joint.angularZMotion = ConfigurableJointMotion.Free;
            col.isTrigger = false;
            withMask = false;

        } else if (playerTrigger)
        {
            if (Input.GetButtonDown("PutOnMask") && !withMask)
            {
                joint.xMotion = ConfigurableJointMotion.Locked;
                joint.yMotion = ConfigurableJointMotion.Locked;
                joint.zMotion = ConfigurableJointMotion.Locked;
                joint.angularXMotion = ConfigurableJointMotion.Locked;
                joint.angularYMotion = ConfigurableJointMotion.Locked;
                joint.angularZMotion = ConfigurableJointMotion.Locked;
                col.isTrigger = true;
                withMask = true;
            }
        }

        if (theaterObjectsList.Count > 0 && withMask)
        {
            for (int i = 0; i < theaterObjectsList.Count; i++)
            {
                GameObject theaterObject = theaterObjectsList[i];
                theaterObject.SetActive(false);
            }
        } else if (theaterObjectsList.Count > 0 && !withMask)
        {
            for (int i = 0; i < theaterObjectsList.Count; i++)
            {
                GameObject theaterObject = theaterObjectsList[i];
                theaterObject.SetActive(true);
            }
        }

        

    }

    void FixedUpdate() {
        joint.connectedAnchor = new Vector3(0f, movementInputScript.headPosition.y, 1f);
    }
	
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            playerTrigger = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            playerTrigger = false;
    }
}
