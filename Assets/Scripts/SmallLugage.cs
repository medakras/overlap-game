﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallLugage : MonoBehaviour {

    public Rigidbody rb;
    public Rigidbody rigidBodyForConfigurableJoint;
    private ConfigurableJoint joint;
    public bool playerTrigger;
    MovementInput movementInputScript;
    public float pushGrab;
    public float mouse;
    private float time = 0;
    private float timeAfter = 0;
    private bool setTime = false;
    public Transform person;
    public bool grabingSmall = false;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        makeConfigurableJoint();

        movementInputScript = FindObjectOfType<MovementInput>();
    }

    void Update()
    {
        pushGrab = Input.GetAxisRaw("PushGrab");
        mouse = Input.GetAxis("Mouse0");

        if (!gameObject.GetComponent<ConfigurableJoint>())
        {
            makeConfigurableJoint();
            setTime = false;
            timeAfter = 0;
        }

        if (playerTrigger)
        {
            if (pushGrab > 0)
            {
                setTime = true;
            }
        }
        if (setTime)
        {
            time += Time.deltaTime;
        } else if (!setTime)
        {
            time = 0;
        }
        if (time > 0.5f)
        {
            joint.xMotion = ConfigurableJointMotion.Locked;
            joint.yMotion = ConfigurableJointMotion.Locked;
            joint.zMotion = ConfigurableJointMotion.Locked;
            joint.angularXMotion = ConfigurableJointMotion.Locked;
            joint.angularYMotion = ConfigurableJointMotion.Locked;
            joint.angularZMotion = ConfigurableJointMotion.Locked;
            grabingSmall = true;

            if (pushGrab == 0)
            {
                timeAfter += Time.deltaTime;
            }
        }
        if (timeAfter > 0.5f)
        {
            joint.xMotion = ConfigurableJointMotion.Free;
            joint.yMotion = ConfigurableJointMotion.Free;
            joint.zMotion = ConfigurableJointMotion.Free;
            joint.angularXMotion = ConfigurableJointMotion.Free;
            joint.angularYMotion = ConfigurableJointMotion.Free;
            joint.angularZMotion = ConfigurableJointMotion.Free;
            grabingSmall = false;
            setTime = false;
            timeAfter = 0;
        }
        if (movementInputScript.toThrow)
        {
            joint.xMotion = ConfigurableJointMotion.Free;
            joint.yMotion = ConfigurableJointMotion.Free;
            joint.zMotion = ConfigurableJointMotion.Free;
            joint.angularXMotion = ConfigurableJointMotion.Free;
            joint.angularYMotion = ConfigurableJointMotion.Free;
            joint.angularZMotion = ConfigurableJointMotion.Free;
            grabingSmall = false;
            setTime = false;
            timeAfter = 0;
            rb.AddForce(person.transform.forward * 100, ForceMode.Impulse);
            movementInputScript.toThrow = false;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (gameObject.GetComponent<ConfigurableJoint>() && time > 0.5f)
            joint.connectedAnchor = new Vector3(0.03f, movementInputScript.handPositionY, 2.6f);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            playerTrigger = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            playerTrigger = false;
    }

    void makeConfigurableJoint()
    {
        joint = gameObject.AddComponent<ConfigurableJoint>();
        joint.connectedBody = rigidBodyForConfigurableJoint;
        joint.autoConfigureConnectedAnchor = false;
        joint.connectedAnchor = new Vector3(0.03f, 4.35f, 2.6f);
        joint.secondaryAxis = new Vector3(0, 0, 0);
        joint.enableCollision = true;
        joint.breakForce = 200000;
        joint.breakTorque = 200000;
    }
}
