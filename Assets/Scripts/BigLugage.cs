﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigLugage : MonoBehaviour {

    private ConfigurableJoint joint;
    public bool flyIn;
    public bool grabBig = false;
    private float pull;

    SmallLugage smallLugageScript;

    public Rigidbody rb;

    // Use this for initialization
    void Start () {
        joint = gameObject.GetComponent<ConfigurableJoint>();
        smallLugageScript = FindObjectOfType<SmallLugage>();
        rb = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        pull = Input.GetAxis("Pull");

        if (flyIn)
        {
            if (pull > 0) {
                joint.xMotion = ConfigurableJointMotion.Locked;
                joint.yMotion = ConfigurableJointMotion.Locked;
                joint.zMotion = ConfigurableJointMotion.Locked;
                joint.angularXMotion = ConfigurableJointMotion.Free;
                joint.angularYMotion = ConfigurableJointMotion.Locked;
                joint.angularZMotion = ConfigurableJointMotion.Locked;
                grabBig = true;
            }
        }
        if (pull == 0)
        {
            joint.xMotion = ConfigurableJointMotion.Free;
            joint.yMotion = ConfigurableJointMotion.Free;
            joint.zMotion = ConfigurableJointMotion.Free;
            joint.angularXMotion = ConfigurableJointMotion.Free;
            joint.angularYMotion = ConfigurableJointMotion.Free;
            joint.angularZMotion = ConfigurableJointMotion.Free;
            grabBig = false;
        }

  
            rb.isKinematic = smallLugageScript.grabingSmall;
     
    }

  

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            flyIn = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            flyIn = false;
    }
}
