﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform target;
    public float smoothSpeed = 1;
    public Vector3 offset;
    private Vector3 velocity = Vector3.zero;

    public float mouseSensitivity = 0;
    public float distanceFromTarget = 2;
    // private Vector2 yawMinMax = new Vector2(0, 25);
    //private Vector2 pitchMinMax = new Vector2(-110, -60);
    private Vector2 pitchMinMax = new Vector2(0, 25);
    private Vector2 yawMinMax = new Vector2(-110, -60);
    

    public float rotationSmoothTime = .12f;
    Vector3 rotationSmoothVelocity;
    Vector3 currentRotation;

    float yaw;
    float pitch;

    public float x;
    public float y;

    public float panSpeed = 10f;
    public float panBorderThickness = 10f;

    /** --------------Other classes-----------------*/
    private MovementInput movementInputScript;
    /** -------------Other classes end--------------*/

    // Use this for initialization
    void Start () {
		movementInputScript = FindObjectOfType<MovementInput>();
	}
	
	// Update is called once per frame
	void Update () {
		
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothSpeed);

     
            transform.position = new Vector3(Mathf.Clamp(smoothedPosition.x, 28f, 46f), smoothedPosition.y, smoothedPosition.z);
        //transform.LookAt(target);

        //Vector3 rot = transform.rotation; 

        //if (Input.mousePosition.y >= Screen.height - panBorderThickness)
        //{
        //    rot.z += panSpeed * Time.deltaTime;
        //}

        //transform.eulerAngles = rot;


        //x = Input.GetAxis("Mouse X");
        //y = Input.GetAxis("Mouse Y");

        //yaw += Input.GetAxis("Mouse X") * mouseSensitivity;
        //yaw = Mathf.Clamp(yaw, yawMinMax.x, yawMinMax.y);
        //pitch += Input.GetAxis("Mouse Y") * mouseSensitivity;
        //pitch = Mathf.Clamp(pitch, pitchMinMax.x, pitchMinMax.y);

        //currentRotation = Vector3.SmoothDamp(currentRotation, new Vector3(pitch, yaw), ref rotationSmoothVelocity, rotationSmoothTime);
        //transform.eulerAngles = currentRotation;

        //transform.position = target.position - transform.forward * distanceFromTarget;
    }


}
